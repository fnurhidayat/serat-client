import { getToken } from "lib"

const NEXT_PUBLIC_SERAT_SERVICE_URL = process.env.NEXT_PUBLIC_SERAT_SERVICE_URL || "https://serat-service.herokuapp.com"

export async function getSerats({ page = 1, pageSize = 10, ...rest } = {}) {
  const query = new URLSearchParams({ page, pageSize, ...rest }).toString();
  const response = await fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/serats?` + query);
  const body = await response.json();

  return body;
}

export async function getSerat(id) {
  const response = await fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/serats/` + id);
  const body = await response.json();

  return body;
}

export async function updateSerat(id, payload) {
  return fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/serats/` + id, {
    method: "PUT",
    headers: {
      Authorization: "Bearer " + getToken(),
    },
    body: JSON.stringify(payload)
  });
}

export async function deleteSerat(id) {
  return fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/serats/` + id, {
    method: "DELETE",
    headers: {
      Authorization: "Bearer " + getToken(),
    }
  });
}
