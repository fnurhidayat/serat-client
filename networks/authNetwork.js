const NEXT_PUBLIC_SERAT_SERVICE_URL = process.env.NEXT_PUBLIC_SERAT_SERVICE_URL || "https://serat-service.herokuapp.com"

export async function postLogin({ username, password }) {
  const response = await fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/auth/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  });

  const body = await response.json();
  return body;
}

export async function getWhoAmI(token) {
  const response = await fetch(`${NEXT_PUBLIC_SERAT_SERVICE_URL}/api/v1/auth/whoami`, {
    headers: {
      Authorization: "Bearer " + token,
    },
  });

  const body = await response.json();
  return body;
}
