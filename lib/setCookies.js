import cookies from "js-cookie"

export default function setCookies(key, value) {
  cookies.set(key, value);
}
