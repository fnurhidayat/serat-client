export default function capitalize(value) {
  let tmp = value.split("");
  tmp[0] = tmp[0].toUpperCase();
  return tmp.join("");
}
