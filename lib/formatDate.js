import dayjs from "dayjs";

export default function formatDate(value) {
  return dayjs(value).format("D MMMM YYYY");
}
