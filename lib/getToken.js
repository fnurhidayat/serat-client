import cookie from "js-cookie"

export default function getToken() {
  return cookie.get("token");
}
