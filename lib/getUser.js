import { getWhoAmI } from "networks"
import getCookies from "./getCookies"

export default async function getUser(req) {
  const { token } = getCookies(req)

  const { status, data } = await getWhoAmI(token);

  if (status === "FAIL") return null; 

  return data;
}
