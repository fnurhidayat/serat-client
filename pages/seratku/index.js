import { useState, useEffect, Fragment } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import Image from "next/image";
import styles from "styles/Home.module.css";
import { getUser } from "lib";
import { getSerats } from "networks";
import Card from "components/card";
import Title from "components/title";
import Description from "components/description";
import Pagination from "components/pagination";

export default function Seratku({ meta, data }) {
  const router = useRouter();
  const { page = "1" } = router.query;
  const [isRefreshing, setIsRefreshing] = useState(false);

  useEffect(() => {
    setIsRefreshing(false);
  }, data.serats);

  async function refreshData() {
    router.replace({
      pathname: "/seratku",
      query: {
        ...router.query,
      },
    });

    setIsRefreshing(true);
  }

  function handleFirstPageClick() {
    router.query.page = "1";
    refreshData();
  }

  function handlePreviousPageClick() {
    let previousPage = Number(page) - 1;

    if (previousPage <= 0) previousPage = meta.pagination.pageCount;
    router.query.page = previousPage.toString();
    refreshData();
  }

  function handleNextPageClick() {
    let nextPage = Number(page) + 1;

    if (nextPage > meta.pagination.pageCount) nextPage = 1;
    router.query.page = nextPage.toString();
    refreshData();
  }

  function handleLastPageClick() {
    router.query.page = meta.pagination.pageCount.toString();
    refreshData();
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Serat</title>
        <meta name="description" content="Read serats!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Your <a href="https://nextjs.org">Serats!</a>
        </h1>

        <p className={styles.description}>
          You can modify them if you want!
        </p>

        {isRefreshing ? (
          <h1>Looking for Serats!...</h1>
        ) : (
          <Fragment>
            <div className={styles.grid}>
              {data.serats.map(({ id, title, body }) => (
                <Card href={`/seratku/${id}`} key={id}>
                  <Title>{title}</Title>
                  <Description>{body}</Description>
                </Card>
              ))}
            </div>

            <Pagination
              page={page}
              pageCount={meta.pagination.pageCount}
              onFirstPageClick={handleFirstPageClick}
              onPreviousPageClick={handlePreviousPageClick}
              onNextPageClick={handleNextPageClick}
              onLastPageClick={handleLastPageClick}
            />
          </Fragment>
        )}
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  );
}

export async function getServerSideProps({ req, query }) {
  const user = await getUser(req);

  if (!user) return {
    redirect: {
      destination: "/",
      permanent: false,
    }
  }

  const { page = 1, pageSize = 10 } = query;
  const { meta, data } = await getSerats({
    page,
    pageSize,
    userId: user.id,
  });

  return {
    props: {
      meta,
      data,
    },
  };
}
