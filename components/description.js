export default function Description({ children }) {
  const text = children.replace(/<[^>]*>?/gm, "");
  const displayedText = text.slice(0, 69);
  return <p>{displayedText}</p>;
}
