export default function Title({ children }) {
  return <h4>{children} &rarr;</h4>;
}
