import styles from "./pagination.module.css";

export default function Pagination({
  page,
  pageCount,
  onFirstPageClick = () => {},
  onLastPageClick = () => {},
  onNextPageClick = () => {},
  onPreviousPageClick = () => {},
}) {
  return (
    <div className={styles.pagination}>
      <button onClick={() => onFirstPageClick(1)}>«</button>
      <button onClick={() => onPreviousPageClick(page - 1)}>‹</button>
      <p>
        Page {page} of {pageCount} pages
      </p>
      <button onClick={() => onNextPageClick(page + 1)}>›</button>
      <button onClick={() => onLastPageClick(pageCount)}>»</button>
    </div>
  );
}
