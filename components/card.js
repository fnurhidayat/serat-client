import Link from "next/link";
import styles from "./card.module.css";

export default function Card({ href, children }) {
  return (
    <Link href={href}>
      <a className={styles.card}>{children}</a>
    </Link>
  );
}
