import styles from "./header.module.css";
import { capitalize, formatDate } from "lib";

export default function Header({ author, children, date }) {
  return (
    <p className={styles.header}>
      By <b>{capitalize(author)}</b> @{" "}
      <span className={styles.date}>{formatDate(date)}</span>
    </p>
  );
}
